package com.amal.microservices.apt.gateway;

import org.springframework.cloud.gateway.route.Route;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.Buildable;
import org.springframework.cloud.gateway.route.builder.PredicateSpec;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.function.Function;

@Configuration
public class ApiGatewayConfiguration {

    @Bean
    public RouteLocator gatewayRouter(RouteLocatorBuilder builder)
    {
        //if a get request comes in, then redirect it
        Function<PredicateSpec, Buildable<Route>> routeFn1 = p -> p.path("/get")
                .filters( f -> f.addRequestHeader("MyHeader","MyURI")
                .addRequestParameter("MyParam","MyValue"))
                .uri("http://httpbin.org:80");

        Function<PredicateSpec, Buildable<Route>> routeFn2 = p -> p.path("/currency-exchange/**")    // <-  if a request containing /currency-exchange comes in
                                                                        .uri("lb://currency-exchange");       // <- load balance and pass it to naming server

        Function<PredicateSpec, Buildable<Route>> routeFn3 = p -> p.path("/currency-conversion/**")
                .uri("lb://currency-conversion");

        Function<PredicateSpec, Buildable<Route>> routeFn4 = p -> p.path("/currency-conversion-feign/**")
                .uri("lb://currency-conversion-feign");

        Function<PredicateSpec, Buildable<Route>> routeFn5 = p -> p.path("/ccn/**")
                .filters( f -> f.rewritePath("/ccn/(?<segment>.*)","/currency-conversion/${segment}"))
                .uri("lb://currency-conversion");

        return builder.routes()
                .route(routeFn1)
                .route(routeFn2)   //  <- for /currency-exchange/currency-exchange to /currency-exchange redirection
                .route(routeFn3)   // <- for /currency-conversion
                .route(routeFn4)   // <- for /currency-conversion-feign
                .route(routeFn5)   // <- adding a custom short path
                .build();
    }

}
